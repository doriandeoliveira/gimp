from gimpfu import *
import os

def export_all_layers(img, path, indice_extensao):
	
	img = img.duplicate()
	lista_extensoes = ['jpg', 'png']
	for layer in img.layers:
		layer.visible = False
	for idx, layer in enumerate(img.layers):
		layer.visible = True
		filename = 'export_' + str(idx) + '_.' + lista_extensoes[indice_extensao]
		fullpath = os.path.join(path, filename)
		layer_img = img.duplicate()
		layer_img.flatten()
		
		layer_drawable = pdb.gimp_image_merge_visible_layers(layer_img, CLIP_TO_IMAGE)
		
		pdb.gimp_file_save(layer_img, layer_drawable, fullpath, filename)
		img.remove_layer(layer)

register(
    "python-fu-export-layers",
    "Exporta cada camada em um arquivo diferente",
    "Exporta cada camada em um arquivo diferente",
    "Dorian Torres",
    "",
    "",
    "Exportar Todas as Camadas",
    "*",
    [
        (PF_IMAGE, "img", "Input image", None),
        (PF_DIRNAME, "path", "Output directory", os.getcwd()),
		(PF_OPTION, "indice_extensao", "Extensao do Arquivo", 0, [".JPG",".PNG"])
    ],
    [],
	export_all_layers,
	menu="<Image>/Meus Plugins/"
    )

main()