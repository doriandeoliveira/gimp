from gimpfu import *
import os

def fit_images_on_layer(img):
	quantidade_camadas = len(img.layers);
	for i in range(0, quantidade_camadas):
		layer = img.layers[i];
		factor = min (float(img.width) / layer.width, float(img.height) / layer.height);
		layer.scale(int(layer.width * factor), int(layer.height * factor));
		layer.set_offsets((img.width - layer.width) / 2, (img.height - layer.height) / 2);

register(
    "python-fu-fit-on-layer",
    "Ajusta a dimensao das imagens de todas as camadas dentro da area de edicao",
    "Ajusta a dimensao das imagens de todas as camadas dentro da area de edicao",
    "Dorian Torres",
    "",
    "",
    "Ajustar Imagens na Camada",
    "*",
	[
		(PF_IMAGE, "img", "Input image", None)
	],
	[],
	fit_images_on_layer,
	menu="<Image>/Meus Plugins/"
    )

main()